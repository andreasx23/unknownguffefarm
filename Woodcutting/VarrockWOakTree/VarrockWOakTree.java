package com.Unknown.bots.UnknownGuffeFarm.Woodcutting.VarrockWOakTree;

import com.Unknown.bots.UnknownGuffeFarm.Utility.CustomPlayerSense;
import com.Unknown.bots.UnknownGuffeFarm.Woodcutting.VarrockWOakTree.Tasks.*;
import com.runemate.game.api.hybrid.input.Mouse;
import com.runemate.game.api.hybrid.local.Skill;
import com.runemate.game.api.hybrid.location.Area;
import com.runemate.game.api.hybrid.location.Coordinate;
import com.runemate.game.api.script.framework.listeners.SkillListener;
import com.runemate.game.api.script.framework.listeners.events.SkillEvent;
import com.runemate.game.api.script.framework.task.TaskBot;

public class VarrockWOakTree extends TaskBot implements SkillListener {

    private final Area INSIDE_BANK_AREA_COORDS, OAK_TREE_AREA, VARROCK_WEST_BANK_AREA, REGULAR_TREE_AREA;
    private int sessionWoodcuttingLevel;
    private int wcXP;
    private int startingWoodcuttingLevel;
    private int wcLevelsGained;
    private int logsCut;

    public VarrockWOakTree() {
        INSIDE_BANK_AREA_COORDS = new Area.Rectangular(new Coordinate(3180, 3446, 0), new Coordinate(3185, 3433, 0));
        OAK_TREE_AREA = new Area.Rectangular(new Coordinate(3157, 3424, 0), new Coordinate(3172, 3409, 0));
        VARROCK_WEST_BANK_AREA = new Area.Rectangular(new Coordinate(3142, 3466, 0), new Coordinate(3193, 3401, 0));
        REGULAR_TREE_AREA = new Area.Rectangular(new Coordinate(3144, 3466, 0), new Coordinate(3172, 3450, 0));
        sessionWoodcuttingLevel = Skill.WOODCUTTING.getCurrentLevel();
        wcXP = 0;
        startingWoodcuttingLevel = Skill.WOODCUTTING.getCurrentLevel();
        wcLevelsGained = 0;
        wcLevelsGained = 0;
        logsCut = 0;
    }

    public Area getINSIDE_BANK_AREA_COORDS() {
        return INSIDE_BANK_AREA_COORDS;
    }

    public Area getOAK_TREE_AREA() {
        return OAK_TREE_AREA;
    }

    public Area getVARROCK_WEST_BANK_AREA() {
        return VARROCK_WEST_BANK_AREA;
    }

    public Area getREGULAR_TREE_AREA() {
        return REGULAR_TREE_AREA;
    }

    public int getWoodcuttingLevel() {
        return Skill.WOODCUTTING.getCurrentLevel();
    }

    /**
     * Experience, planks used & objects made tracker
     */
    public void onExperienceGained(SkillEvent event) {
        sessionWoodcuttingLevel = Skill.WOODCUTTING.getCurrentLevel();

        if (event.getChange() > 0) {
            wcXP += event.getChange();
            logsCut++;
        }

        if (sessionWoodcuttingLevel > startingWoodcuttingLevel) {
            wcLevelsGained = sessionWoodcuttingLevel - startingWoodcuttingLevel;
        }

        System.out.println("WC XP gained: " + wcXP);
        System.out.println("Session level: " + sessionWoodcuttingLevel);
        System.out.println("Starting level: " + startingWoodcuttingLevel);
        System.out.println("Levels gained: " + wcLevelsGained);
        System.out.println("Logs cut: " + logsCut);
        System.out.println("");
    }

    @Override
    public void onStart(String... args){
        getEventDispatcher().addListener(this);
        CustomPlayerSense.initializeKeys();
        Mouse.setPathGenerator(Mouse.MLP_PATH_GENERATOR);

        setLoopDelay(123, 324);
        add(new Banking(), new TraversalToRegularTree(), new TraversalToOakTree(), new CutTree(), new TraversalToBank());
    }

}
