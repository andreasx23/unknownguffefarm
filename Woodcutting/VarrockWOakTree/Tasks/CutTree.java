package com.Unknown.bots.UnknownGuffeFarm.Woodcutting.VarrockWOakTree.Tasks;

import com.Unknown.bots.UnknownGuffeFarm.Utility.CommonAPICalls;
import com.Unknown.bots.UnknownGuffeFarm.Woodcutting.VarrockWOakTree.VarrockWOakTree;
import com.runemate.game.api.hybrid.entities.GameObject;
import com.runemate.game.api.hybrid.entities.Player;
import com.runemate.game.api.hybrid.local.Camera;
import com.runemate.game.api.hybrid.local.hud.interfaces.Inventory;
import com.runemate.game.api.hybrid.region.GameObjects;
import com.runemate.game.api.hybrid.region.Players;
import com.runemate.game.api.script.Execution;
import com.runemate.game.api.script.framework.task.Task;

import java.util.Random;

public class CutTree extends Task {

    private VarrockWOakTree bot;
    private Player player;
    private GameObject tree;
    private CommonAPICalls commonAPICalls;
    private Random random;

    public CutTree() {
        bot = new VarrockWOakTree();
        player = Players.getLocal();
        commonAPICalls = new CommonAPICalls();
        random = new Random();
    }

    private void chooseTreeToCut(String treeType, Player myPlayer) {
        tree = GameObjects.newQuery().names(treeType).results().nearestTo(myPlayer);
        if (tree != null) {
            if (!tree.isVisible()) {
                Camera.turnTo(tree);
            } else {
                tree.interact("Chop down");
                Execution.delayUntil(commonAPICalls.PLAYER_ANIMATING, 3850, 5500);
            }
        }
    }

    @Override
    public boolean validate() {
        if (player == null) {
            return false;
        }

        if (bot.getWoodcuttingLevel() < 15) {
            tree = GameObjects.newQuery().names("Tree").results().nearestTo(player);
            if (tree == null) {
                return false;
            }
            return bot.getREGULAR_TREE_AREA().contains(player) && !GameObjects.newQuery().names("Tree").actions("Chop down").results().isEmpty() &&
                    bot.getVARROCK_WEST_BANK_AREA().contains(player) && !Inventory.isFull() &&
                    Inventory.containsAnyOf("Bronze axe", "Iron axe", "Steel axe", "Mithril axe", "Adamant axe", "Rune axe") && player.getAnimationId() == -1 && bot.getVARROCK_WEST_BANK_AREA().contains(tree);
        } else {
            tree = GameObjects.newQuery().names("Oak").results().nearestTo(player);
            if (tree == null) {
                return false;
            }
            return bot.getOAK_TREE_AREA().contains(player) && !GameObjects.newQuery().names("Oak").actions("Chop down").results().isEmpty() &&
                    bot.getVARROCK_WEST_BANK_AREA().contains(player) && !Inventory.isFull() &&
                    Inventory.containsAnyOf("Bronze axe", "Iron axe", "Steel axe", "Mithril axe", "Adamant axe", "Rune axe") && player.getAnimationId() == -1 && bot.getVARROCK_WEST_BANK_AREA().contains(tree);
        }
    }

    @Override
    public void execute() {
        getLogger().debug("Cutting tree");

        if (player != null) {
            if (bot.getWoodcuttingLevel() < 15) {
                chooseTreeToCut("Tree", player);
            } else {
                chooseTreeToCut("Oak", player);
            }
        }

    }

}