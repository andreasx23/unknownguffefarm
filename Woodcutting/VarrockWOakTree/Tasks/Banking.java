package com.Unknown.bots.UnknownGuffeFarm.Woodcutting.VarrockWOakTree.Tasks;

import com.Unknown.bots.UnknownGuffeFarm.Utility.CustomPlayerSense;
import com.Unknown.bots.UnknownGuffeFarm.Woodcutting.VarrockWOakTree.VarrockWOakTree;
import com.runemate.game.api.hybrid.entities.GameObject;
import com.runemate.game.api.hybrid.entities.Player;
import com.runemate.game.api.hybrid.local.Camera;
import com.runemate.game.api.hybrid.local.hud.interfaces.Bank;
import com.runemate.game.api.hybrid.local.hud.interfaces.Inventory;
import com.runemate.game.api.hybrid.region.GameObjects;
import com.runemate.game.api.hybrid.region.Players;
import com.runemate.game.api.script.Execution;
import com.runemate.game.api.script.framework.task.Task;

import java.util.Random;

public class Banking extends Task {

    private VarrockWOakTree bot;
    private Player player;
    private Random random;
    private GameObject bank;

    public Banking() {
        bot = new VarrockWOakTree();
        player = Players.getLocal();
        random = new Random();
    }

    private void withdrawItem(int playerSense_REACTION_TIME, String itemToWithdraw, int itemAmountToWithdraw) {
        getLogger().debug("Withdrawing: " + itemToWithdraw);
        Execution.delay(playerSense_REACTION_TIME);
        Bank.withdraw(itemToWithdraw, itemAmountToWithdraw);
        Execution.delayUntil(() -> Inventory.contains(itemToWithdraw), 600, 1200);
    }

    @Override
    public boolean validate() {
        if (player == null) {
            return false;
        }

        return bot.getINSIDE_BANK_AREA_COORDS().contains(player) && bot.getVARROCK_WEST_BANK_AREA().contains(player) && Inventory.isFull() ||

                bot.getINSIDE_BANK_AREA_COORDS().contains(player) && bot.getVARROCK_WEST_BANK_AREA().contains(player) &&
                        !Inventory.containsAnyOf("Bronze axe", "Iron axe", "Steel axe", "Mithril axe", "Adamant axe", "Rune axe");
    }

    @Override
    public void execute() {
        getLogger().debug("Banking");
        bank = GameObjects.newQuery().names("Bank booth").results().nearestTo(player);
        int REACTION_TIME = CustomPlayerSense.Key.REACION_TIME.getAsInteger();

        if (bank != null) {
            if (!bank.isVisible()) {
                Camera.turnTo(bank);
            } else {
                if (!Bank.isOpen()) {
                    Bank.open();
                } else {
                    Bank.depositAllExcept("Bronze axe", "Iron axe", "Steel axe", "Mithril axe", "Adamant axe", "Rune axe");
                    Execution.delay(500, 1250);

                    if (!Inventory.contains("Iron axe") && bot.getWoodcuttingLevel() < 6) {
                        withdrawItem(REACTION_TIME, "Iron axe", 1);
                    }
                    if (Inventory.contains("Iron axe") && bot.getWoodcuttingLevel() >= 6 && bot.getWoodcuttingLevel() < 21) {
                        withdrawItem(REACTION_TIME, "Steel axe", 1);
                        Execution.delay(1000, 2000);
                        Bank.deposit("Iron axe", 1);
                        Execution.delayUntil(() -> !Inventory.contains("Iron axe"), 1800, 2400);
                    }
                    if (Inventory.contains("Steel axe") && bot.getWoodcuttingLevel() >= 21 && bot.getWoodcuttingLevel() < 31) {
                        withdrawItem(REACTION_TIME, "Mithril axe", 1);
                        Execution.delay(1000, 2000);
                        Bank.deposit("Steel axe", 1);
                        Execution.delayUntil(() -> !Inventory.contains("Steel axe"), 1800, 2400);
                    }
                    if (Inventory.contains("Mithril axe") && bot.getWoodcuttingLevel() >= 31 && bot.getWoodcuttingLevel() < 41) {
                        withdrawItem(REACTION_TIME, "Adamant axe", 1);
                        Execution.delay(1000, 2000);
                        Bank.deposit("Mithril axe", 1);
                        Execution.delayUntil(() -> !Inventory.contains("Mithril axe"), 1800, 2400);
                    }
                    if (Inventory.contains("Adamant axe") && bot.getWoodcuttingLevel() >= 41) {
                        withdrawItem(REACTION_TIME, "Rune axe", 1);
                        Execution.delay(1000, 2000);
                        Bank.deposit("Adamant axe", 1);
                        Execution.delayUntil(() -> !Inventory.contains("Adamant axe"), 1800, 2400);
                    }

                    if (!Inventory.contains("Oak logs") || !Inventory.contains("Logs")) {
                        Execution.delay(REACTION_TIME, random.nextInt(1500 + 1 - REACTION_TIME) + REACTION_TIME);
                        if (CustomPlayerSense.Key.USE_ESCAPE_TO_CLOSE_BANK.getAsBoolean()) {
                            getLogger().debug("USED ESCAPE!");
                            Bank.close(true);
                            Execution.delayUntil(() -> !Bank.isOpen(), 600, 1800);
                        } else {
                            getLogger().debug("MANUALLY CLOSED!");
                            Bank.close(false);
                            Execution.delayUntil(() -> !Bank.isOpen(), 600, 1800);
                        }
                    }
                }
            }
        }

    }

}