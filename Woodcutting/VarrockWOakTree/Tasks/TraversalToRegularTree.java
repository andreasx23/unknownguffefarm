package com.Unknown.bots.UnknownGuffeFarm.Woodcutting.VarrockWOakTree.Tasks;

import com.Unknown.bots.UnknownGuffeFarm.Utility.CommonAPICalls;
import com.Unknown.bots.UnknownGuffeFarm.Woodcutting.VarrockWOakTree.VarrockWOakTree;
import com.runemate.game.api.hybrid.entities.Player;
import com.runemate.game.api.hybrid.local.hud.interfaces.Inventory;
import com.runemate.game.api.hybrid.location.navigation.Traversal;
import com.runemate.game.api.hybrid.location.navigation.web.WebPath;
import com.runemate.game.api.hybrid.region.Players;
import com.runemate.game.api.script.Execution;
import com.runemate.game.api.script.framework.task.Task;

public class TraversalToRegularTree extends Task {

    private VarrockWOakTree bot;
    private Player player;
    private CommonAPICalls commonAPICalls;

    public TraversalToRegularTree() {
        bot = new VarrockWOakTree();
        player = Players.getLocal();
        commonAPICalls = new CommonAPICalls();
    }

    @Override
    public boolean validate() {
        if (player == null) {
            return false;
        }

        return bot.getVARROCK_WEST_BANK_AREA().contains(player) && !bot.getREGULAR_TREE_AREA().contains(player) && !Inventory.isFull() && bot.getWoodcuttingLevel() < 15
                && Inventory.containsAnyOf("Bronze axe", "Iron axe", "Steel axe", "Mithril axe", "Adamant axe", "Rune axe");
    }

    @Override
    public void execute() {
        getLogger().debug("Traversal to regular tree");
        final WebPath path = Traversal.getDefaultWeb().getPathBuilder().buildTo(bot.getREGULAR_TREE_AREA().getRandomCoordinate());

        if (path != null) { // IMPORTANT: if the path should be null, the pathbuilder could not manage to build a path with the given web, so always nullcheck!
            path.step();
            Execution.delayWhile(commonAPICalls.PLAYER_MOVING, 750, 1500);
        }
    }

}