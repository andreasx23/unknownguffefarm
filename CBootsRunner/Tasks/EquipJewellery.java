package com.Unknown.bots.UnknownGuffeFarm.CBootsRunner.Tasks;

import com.Unknown.bots.UnknownGuffeFarm.CBootsRunner.CBootsRunner;
import com.Unknown.bots.UnknownGuffeFarm.Utility.CustomPlayerSense;
import com.runemate.game.api.hybrid.entities.Player;
import com.runemate.game.api.hybrid.local.hud.interfaces.Bank;
import com.runemate.game.api.hybrid.local.hud.interfaces.Equipment;
import com.runemate.game.api.hybrid.local.hud.interfaces.Inventory;
import com.runemate.game.api.hybrid.local.hud.interfaces.SpriteItem;
import com.runemate.game.api.hybrid.region.Players;
import com.runemate.game.api.script.Execution;
import com.runemate.game.api.script.framework.task.Task;

import java.util.Random;

public class EquipJewellery extends Task {

    private CBootsRunner bot;
    private Player player;
    private SpriteItem necklace, ring;

    public EquipJewellery() {
        bot = new CBootsRunner();
        player = Players.getLocal();
    }

    @Override
    public boolean validate() {
        if (player == null) {
            return false;
        }

        return Inventory.containsAnyOf("Games necklace(8)", "Ring of dueling(8)");
    }

    @Override
    public void execute() {
        necklace = Inventory.newQuery().names("Games necklace(8)").results().first();
        ring = Inventory.newQuery().names("Ring of dueling(8)").results().first();

        if (!Bank.isOpen()) {
            if (necklace != null) {
                if (Inventory.containsAnyOf("Games necklace(8)", "Games necklace(7)", "Games necklace(6)", "Games necklace(5)", "Games necklace(4)", "Games necklace(3)", "Games necklace(2)", "Games necklace(1)") &&
                        !Equipment.containsAnyOf("Games necklace(8)", "Games necklace(7)", "Games necklace(6)", "Games necklace(5)", "Games necklace(4)", "Games necklace(3)", "Games necklace(2)", "Games necklace(1)")) {
                    necklace.interact("Wear");
                    getLogger().debug("Equipping: Games necklace(8)");
                    Execution.delayUntil(() -> necklace.isValid(), 600, 1800);
                }
            }

            if (ring != null) {
                if (Inventory.containsAnyOf("Ring of dueling(8)", "Ring of dueling(7)", "Ring of dueling(6)", "Ring of dueling(5)", "Ring of dueling(4)", "Ring of dueling(3)", "Ring of dueling(2)", "Ring of dueling(1)") &&
                        !Equipment.containsAnyOf("Ring of dueling(8)", "Ring of dueling(7)", "Ring of dueling(6)", "Ring of dueling(5)", "Ring of dueling(4)", "Ring of dueling(3)", "Ring of dueling(2)", "Ring of dueling(1)")) {
                    ring.interact("Wear");
                    getLogger().debug("Equipping: Ring of dueling(8)");
                    Execution.delayUntil(() -> ring.isValid(), 600, 1800);
                }
            }
        }


    }

}