package com.Unknown.bots.UnknownGuffeFarm.CBootsRunner.Tasks;

import com.Unknown.bots.UnknownGuffeFarm.CBootsRunner.CBootsRunner;
import com.Unknown.bots.UnknownGuffeFarm.Utility.CustomPlayerSense;
import com.runemate.game.api.hybrid.entities.Player;
import com.runemate.game.api.hybrid.local.Camera;
import com.runemate.game.api.hybrid.local.hud.interfaces.Bank;
import com.runemate.game.api.hybrid.local.hud.interfaces.Equipment;
import com.runemate.game.api.hybrid.local.hud.interfaces.Inventory;
import com.runemate.game.api.hybrid.local.hud.interfaces.SpriteItem;
import com.runemate.game.api.hybrid.region.Players;
import com.runemate.game.api.script.Execution;
import com.runemate.game.api.script.framework.task.Task;

import java.util.Random;

public class Banking extends Task {

    private CBootsRunner bot;
    private Player player;
    private SpriteItem necklace, ring;
    private Random random;

    public Banking() {
        bot = new CBootsRunner();
        player = Players.getLocal();
        random = new Random();
    }

    private void withdrawItem(int playerSense_REACTION_TIME, String itemToWithdraw, int itemAmountToWithdraw) {
        getLogger().debug("Withdrawing: " + itemToWithdraw);
        Execution.delay(playerSense_REACTION_TIME);
        Bank.withdraw(itemToWithdraw, itemAmountToWithdraw);
        Execution.delayUntil(() -> Inventory.contains(itemToWithdraw), 600, 1200);
    }

    @Override
    public boolean validate() {
        if (player == null) {
            return false;
        }

        return bot.getBANK_AREA_COORDS().contains(player) &&
                !bot.getPORTAL_OUTSIDE_COORDS().contains(player) &&
                !bot.getPORTAL_INSIDE_COORDS().contains(player) &&
                !bot.getBURTHORPE_COORDS().contains(player) &&
                !bot.getTENZING_HOUSE_COORDS().contains(player) &&
                bot.getCLAN_WARS_TP_COORDS().contains(player) &&

                bot.getCLAN_WARS_AREA().contains(player) &&
                !bot.getBURTHORPE_AREA().contains(player) &&
                Inventory.isFull() ||

                bot.getBANK_AREA_COORDS().contains(player) &&
                        !bot.getPORTAL_OUTSIDE_COORDS().contains(player) &&
                        !bot.getPORTAL_INSIDE_COORDS().contains(player) &&
                        !bot.getBURTHORPE_COORDS().contains(player) &&
                        !bot.getTENZING_HOUSE_COORDS().contains(player) &&
                        bot.getCLAN_WARS_TP_COORDS().contains(player) &&

                        bot.getCLAN_WARS_AREA().contains(player) &&
                        !bot.getBURTHORPE_AREA().contains(player) &&
                        !Equipment.containsAnyOf("Games necklace(8)", "Games necklace(7)", "Games necklace(6)", "Games necklace(5)", "Games necklace(4)", "Games necklace(3)", "Games necklace(2)", "Games necklace(1)") ||

                bot.getBANK_AREA_COORDS().contains(player) &&
                        !bot.getPORTAL_OUTSIDE_COORDS().contains(player) &&
                        !bot.getPORTAL_INSIDE_COORDS().contains(player) &&
                        !bot.getBURTHORPE_COORDS().contains(player) &&
                        !bot.getTENZING_HOUSE_COORDS().contains(player) &&
                        bot.getCLAN_WARS_TP_COORDS().contains(player) &&

                        bot.getCLAN_WARS_AREA().contains(player) &&
                        !bot.getBURTHORPE_AREA().contains(player) &&
                        !Equipment.containsAnyOf("Ring of dueling(8)", "Ring of dueling(7)", "Ring of dueling(6)", "Ring of dueling(5)", "Ring of dueling(4)", "Ring of dueling(3)", "Ring of dueling(2)", "Ring of dueling(1)");
    }

    @Override
    public void execute() {
        getLogger().debug("Banking");
        int REACTION_TIME = CustomPlayerSense.Key.REACION_TIME.getAsInteger();

        if (!Bank.isOpen()) {
            Bank.open();
        } else {
            if (bot.isUseStam()) {
                Bank.depositAllExcept("Stamina potion(4)", "Stamina potion(3)", "Stamnia potion(2)", "Stamina potion(1)", "Coins");

                if (!Inventory.containsAnyOf("Stamina potion(4)", "Stamina potion(3)", "Stamnia potion(2)", "Stamina potion(1)")) {
                    withdrawItem(REACTION_TIME, "Stamina potion(4)", 1);
                }
            } else {
                Bank.depositAllExcept("Coins");
            }

            if (!Equipment.containsAnyOf("Games necklace(8)", "Games necklace(7)", "Games necklace(6)", "Games necklace(5)", "Games necklace(4)", "Games necklace(3)", "Games necklace(2)", "Games necklace(1)")) {
                withdrawItem(REACTION_TIME, "Games necklace(8)", 1);
            }

            if (!Equipment.containsAnyOf("Ring of dueling(8)", "Ring of dueling(7)", "Ring of dueling(6)", "Ring of dueling(5)", "Ring of dueling(4)", "Ring of dueling(3)", "Ring of dueling(2)", "Ring of dueling(1)")) {
                withdrawItem(REACTION_TIME, "Ring of dueling(8)", 1);
            }

            if (!Inventory.contains("Climbing boots")) {
                Execution.delay(REACTION_TIME, random.nextInt(1500 + 1 - REACTION_TIME) + REACTION_TIME);
                if (CustomPlayerSense.Key.USE_ESCAPE_TO_CLOSE_BANK.getAsBoolean()) {
                    getLogger().debug("USED ESCAPE!");
                    Bank.close(true);
                    Execution.delayUntil(() -> !Bank.isOpen(), 600, 1800);
                } else {
                    getLogger().debug("MANUALLY CLOSED!");
                    Bank.close(false);
                    Execution.delayUntil(() -> !Bank.isOpen(), 600, 1800);
                }
            }
        }

    }

}