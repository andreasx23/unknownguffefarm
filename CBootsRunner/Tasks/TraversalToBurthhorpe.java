package com.Unknown.bots.UnknownGuffeFarm.CBootsRunner.Tasks;

import com.Unknown.bots.UnknownGuffeFarm.CBootsRunner.CBootsRunner;
import com.runemate.game.api.hybrid.entities.Player;
import com.runemate.game.api.hybrid.local.hud.interfaces.Equipment;
import com.runemate.game.api.hybrid.local.hud.interfaces.SpriteItem;
import com.runemate.game.api.hybrid.region.Players;
import com.runemate.game.api.script.Execution;
import com.runemate.game.api.script.framework.task.Task;

public class TraversalToBurthhorpe extends Task {

    private CBootsRunner bot;
    private Player player;
    private SpriteItem necklace;

    public TraversalToBurthhorpe() {
        bot = new CBootsRunner();
        player = Players.getLocal();
    }

    @Override
    public boolean validate() {
        if (player == null) {
            return false;
        }

        return !bot.getBANK_AREA_COORDS().contains(player) &&
                !bot.getPORTAL_OUTSIDE_COORDS().contains(player) &&
                bot.getPORTAL_INSIDE_COORDS().contains(player) &&
                !bot.getBURTHORPE_COORDS().contains(player) &&
                !bot.getTENZING_HOUSE_COORDS().contains(player) &&
                !bot.getCLAN_WARS_TP_COORDS().contains(player) &&

                !bot.getCLAN_WARS_AREA().contains(player) &&
                !bot.getBURTHORPE_AREA().contains(player) &&
                !bot.isCBootsInInv();
    }

    @Override
    public void execute() {
        getLogger().debug("Teleporting to Burthorpe");
        necklace = Equipment.newQuery().names("Games necklace(8)", "Games necklace(7)", "Games necklace(6)", "Games necklace(5)", "Games necklace(4)", "Games necklace(3)", "Games necklace(2)", "Games necklace(1)").results().first();

        if (necklace != null) {
            if (necklace.interact("Burthorpe")) {
                Execution.delayUntil(() -> bot.getBURTHORPE_COORDS().contains(player), 2500, 5000);
            }
        }

    }

}