package com.Unknown.bots.UnknownGuffeFarm.CBootsRunner.Tasks;

import com.Unknown.bots.UnknownGuffeFarm.CBootsRunner.CBootsRunner;
import com.runemate.game.api.hybrid.entities.GameObject;
import com.runemate.game.api.hybrid.entities.Player;
import com.runemate.game.api.hybrid.region.GameObjects;
import com.runemate.game.api.hybrid.region.Players;
import com.runemate.game.api.script.Execution;
import com.runemate.game.api.script.framework.task.Task;

public class EnterPortal extends Task {

    private CBootsRunner bot;
    private Player player;
    private GameObject portal;

    public EnterPortal() {
        bot = new CBootsRunner();
        player = Players.getLocal();
    }

    @Override
    public boolean validate() {
        if (player == null) {
            return false;
        }

        return !bot.getBANK_AREA_COORDS().contains(player) &&
                bot.getPORTAL_OUTSIDE_COORDS().contains(player) &&
                !bot.getPORTAL_INSIDE_COORDS().contains(player) &&
                !bot.getBURTHORPE_COORDS().contains(player) &&
                !bot.getTENZING_HOUSE_COORDS().contains(player) &&
                !bot.getCLAN_WARS_TP_COORDS().contains(player) &&

                bot.getCLAN_WARS_AREA().contains(player) &&
                !bot.getBURTHORPE_AREA().contains(player) &&
                !bot.isCBootsInInv();
    }

    @Override
    public void execute() {
        getLogger().debug("Entering portal");
        portal = GameObjects.newQuery().names("Free-for-all portal").results().first();

        if (portal != null) {
            if (portal.interact("Enter")) {
                Execution.delayUntil(() -> bot.getPORTAL_INSIDE_COORDS().contains(player), 600, 1800);
            }
        }

    }

}