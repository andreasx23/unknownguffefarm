package com.Unknown.bots.UnknownGuffeFarm.CBootsRunner.Tasks;

import com.Unknown.bots.UnknownGuffeFarm.CBootsRunner.CBootsRunner;
import com.runemate.game.api.hybrid.entities.Player;
import com.runemate.game.api.hybrid.local.hud.interfaces.Equipment;
import com.runemate.game.api.hybrid.local.hud.interfaces.Inventory;
import com.runemate.game.api.hybrid.local.hud.interfaces.SpriteItem;
import com.runemate.game.api.hybrid.region.Players;
import com.runemate.game.api.script.Execution;
import com.runemate.game.api.script.framework.task.Task;

public class TraversalToClanWars extends Task {

    private CBootsRunner bot;
    private Player player;
    private SpriteItem ring;

    public TraversalToClanWars() {
        bot = new CBootsRunner();
        player = Players.getLocal();
    }

    @Override
    public boolean validate() {
        if (player == null) {
            return false;
        }

        return !bot.getBANK_AREA_COORDS().contains(player) &&
                !bot.getPORTAL_OUTSIDE_COORDS().contains(player) &&
                !bot.getPORTAL_INSIDE_COORDS().contains(player) &&
                !bot.getBURTHORPE_COORDS().contains(player) &&
                bot.getTENZING_HOUSE_COORDS().contains(player) &&
                !bot.getCLAN_WARS_TP_COORDS().contains(player) &&

                !bot.getCLAN_WARS_AREA().contains(player) &&
                bot.getBURTHORPE_AREA().contains(player) &&
                Inventory.isFull();
    }

    @Override
    public void execute() {
        getLogger().debug("Teleporting to Clan Wars");
        ring = Equipment.newQuery().names("Ring of dueling(8)", "Ring of dueling(7)", "Ring of dueling(6)", "Ring of dueling(5)", "Ring of dueling(4)", "Ring of dueling(3)", "Ring of dueling(2)", "Ring of dueling(1)").results().first();

        if (ring != null) {
            if (ring.interact("Clan Wars")) {
                Execution.delayUntil(() -> bot.getCLAN_WARS_TP_COORDS().contains(player), 2500, 5000);
            }
        }

    }

}