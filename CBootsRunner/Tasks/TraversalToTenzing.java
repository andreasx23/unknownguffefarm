package com.Unknown.bots.UnknownGuffeFarm.CBootsRunner.Tasks;

import com.Unknown.bots.UnknownGuffeFarm.CBootsRunner.CBootsRunner;
import com.Unknown.bots.UnknownGuffeFarm.Utility.CommonAPICalls;
import com.Unknown.bots.UnknownGuffeFarm.Utility.CustomPlayerSense;
import com.runemate.game.api.hybrid.entities.GameObject;
import com.runemate.game.api.hybrid.entities.Player;
import com.runemate.game.api.hybrid.input.Keyboard;
import com.runemate.game.api.hybrid.local.Camera;
import com.runemate.game.api.hybrid.local.hud.interfaces.InterfaceComponent;
import com.runemate.game.api.hybrid.local.hud.interfaces.Interfaces;
import com.runemate.game.api.hybrid.location.Coordinate;
import com.runemate.game.api.hybrid.location.navigation.Traversal;
import com.runemate.game.api.hybrid.location.navigation.basic.PredefinedPath;
import com.runemate.game.api.hybrid.location.navigation.web.WebPath;
import com.runemate.game.api.hybrid.region.GameObjects;
import com.runemate.game.api.hybrid.region.Players;
import com.runemate.game.api.script.Execution;
import com.runemate.game.api.script.framework.task.Task;

import java.awt.event.KeyEvent;

public class TraversalToTenzing extends Task {

    private CBootsRunner bot;
    private Player player;
    private CommonAPICalls commonAPICalls;
    private Coordinate doorCoordinate;
    private int doorId;
    private Coordinate gateCoordinate;
    private int gateId;
    private int inventoryId;

//    private final Coordinate[] pathCoords = {new Coordinate(1111, 1111).randomize(2, 2), new Coordinate(2883, 3552).randomize(2, 2),
//            new Coordinate(2871, 3562).randomize(2, 2), new Coordinate(2855, 3568).randomize(2, 2), new Coordinate(2844, 3580).randomize(2, 2),
//            new Coordinate(2829, 3583).randomize(2, 2), new Coordinate(2833, 3570).randomize(2, 2), new Coordinate(2829, 3556).randomize(2, 2)};

//    private final Coordinate[] pathCoords = {new Coordinate(1111, 1111), new Coordinate(2883, 3552),
//            new Coordinate(2871, 3562), new Coordinate(2855, 3568), new Coordinate(2844, 3580),
//            new Coordinate(2829, 3583), new Coordinate(2833, 3570), new Coordinate(2829, 3556)};

    public TraversalToTenzing() {
        bot = new CBootsRunner();
        player = Players.getLocal();
        commonAPICalls = new CommonAPICalls();

        doorCoordinate = new Coordinate(2822, 3555, 0);
        doorId = 0;
        gateCoordinate = new Coordinate(2824, 3555, 0);
        gateId = 0;
        inventoryId = 0;
    }

    private GameObject getDoorObject() {
        GameObject door;
        if (doorId != 0) {
            if ((door = GameObjects.getLoadedOn(doorCoordinate, doorId).first()) != null) {
                return door;
            }
        } else {
            if ((door = GameObjects.newQuery().names("Door").on(doorCoordinate).results().first()) != null) {
                doorId = door.getId();
                return door;
            }
        }
        return null;
    }

    private GameObject getGateObject() {
        GameObject gate;
        if (gateId != 0) {
            if ((gate = GameObjects.getLoadedOn(gateCoordinate, gateId).first()) != null) {
                return gate;
            }
        } else {
            if ((gate = GameObjects.newQuery().names("Gate").on(gateCoordinate).results().first()) != null) {
                gateId = gate.getId();
                return gate;
            }
        }
        return null;
    }

    private InterfaceComponent getInventoryComponent() {
        InterfaceComponent comp;
        if (inventoryId != 0) {
            if ((comp = Interfaces.getAt(548, inventoryId)) != null) {
                return comp;
            }
        } else {
            if ((comp = Interfaces.newQuery().containers(548).actions("Inventory").results().first()) != null) {
                inventoryId = comp.getIndex();
                return comp;
            }
        }
        return null;
    }

    private void clickInterfaceComponent() {
        if (CustomPlayerSense.Key.USE_ESCAPE_TO_OPEN_INV_TAP.getAsBoolean()) {
            getLogger().debug("Showing inventory interface by escape");
            Keyboard.pressKey(KeyEvent.VK_ESCAPE);
        } else {
            if (getInventoryComponent() != null) {
                getLogger().debug("Showing inventory interface by clicking");
                getInventoryComponent().interact("Inventory");
            }
        }
    }

    @Override
    public boolean validate() {
        if (player == null) {
            return false;
        }

        return !bot.getBANK_AREA_COORDS().contains(player) &&
                !bot.getPORTAL_OUTSIDE_COORDS().contains(player) &&
                !bot.getPORTAL_INSIDE_COORDS().contains(player) &&
                bot.getBURTHORPE_COORDS().contains(player) &&
                !bot.getTENZING_HOUSE_COORDS().contains(player) &&
                !bot.getCLAN_WARS_TP_COORDS().contains(player) &&

                !bot.getCLAN_WARS_AREA().contains(player) &&
                bot.getBURTHORPE_AREA().contains(player) &&
                !bot.isCBootsInInv() ||

                !bot.getBANK_AREA_COORDS().contains(player) &&
                        !bot.getPORTAL_OUTSIDE_COORDS().contains(player) &&
                        !bot.getPORTAL_INSIDE_COORDS().contains(player) &&
                        !bot.getBURTHORPE_COORDS().contains(player) &&
                        !bot.getTENZING_HOUSE_COORDS().contains(player) &&
                        !bot.getCLAN_WARS_TP_COORDS().contains(player) &&

                        !bot.getCLAN_WARS_AREA().contains(player) &&
                        bot.getBURTHORPE_AREA().contains(player) &&
                        !bot.isCBootsInInv();
    }

    @Override
    public void execute() {
        getLogger().debug("Traversal to Tenzing");
        WebPath path = Traversal.getDefaultWeb().getPathBuilder().buildTo(bot.getTENZING_HOUSE_COORDS().getRandomCoordinate());

        try {
            if (getGateObject() != null && getGateObject().isVisible() && getGateObject().interact("Open")) {
                getLogger().debug("Opening gate");
                Execution.delayUntil(() -> !getGateObject().isValid(), 2500, 5000);
            } else {
                if (getDoorObject() != null && getDoorObject().isVisible()) {
                    getLogger().debug("Door visible");
                    clickInterfaceComponent();
                    if (getDoorObject().interact("Open")) {
                        getLogger().debug("Opening door");
                        Execution.delayUntil(() -> bot.getTENZING_HOUSE_COORDS().contains(player), 2500, 5000);
                    }
                }
            }
        } catch (NullPointerException e) {
            e.printStackTrace();
        }

//        try {
//            PredefinedPath.create(pathCoords).step();
//        } catch (NullPointerException e) {
//            e.printStackTrace();
//        }

        if (path != null) { // IMPORTANT: if the path should be null, the pathbuilder could not manage to build a path with the given web, so always nullcheck!
            path.step();
            Execution.delayWhile(commonAPICalls.PLAYER_MOVING, 750, 1500);
        }

    }

}