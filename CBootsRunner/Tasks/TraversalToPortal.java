package com.Unknown.bots.UnknownGuffeFarm.CBootsRunner.Tasks;

import com.Unknown.bots.UnknownGuffeFarm.CBootsRunner.CBootsRunner;
import com.Unknown.bots.UnknownGuffeFarm.Utility.CommonAPICalls;
import com.runemate.game.api.hybrid.entities.Player;
import com.runemate.game.api.hybrid.local.hud.interfaces.Equipment;
import com.runemate.game.api.hybrid.location.navigation.Traversal;
import com.runemate.game.api.hybrid.location.navigation.basic.BresenhamPath;
import com.runemate.game.api.hybrid.location.navigation.web.WebPath;
import com.runemate.game.api.hybrid.region.Players;
import com.runemate.game.api.script.Execution;
import com.runemate.game.api.script.framework.task.Task;

public class TraversalToPortal extends Task {

    private CBootsRunner bot;
    private Player player;
    private CommonAPICalls commonAPICalls;

    public TraversalToPortal() {
        bot = new CBootsRunner();
        player = Players.getLocal();
        commonAPICalls = new CommonAPICalls();
    }

    @Override
    public boolean validate() {
        if (player == null) {
            return false;
        }

        return bot.getBANK_AREA_COORDS().contains(player) &&
                !bot.getPORTAL_OUTSIDE_COORDS().contains(player) &&
                !bot.getPORTAL_INSIDE_COORDS().contains(player) &&
                !bot.getBURTHORPE_COORDS().contains(player) &&
                !bot.getTENZING_HOUSE_COORDS().contains(player) &&
                bot.getCLAN_WARS_TP_COORDS().contains(player) &&

                bot.getCLAN_WARS_AREA().contains(player) &&
                !bot.getBURTHORPE_AREA().contains(player) &&
                !bot.isCBootsInInv() &&
                Equipment.containsAnyOf("Games necklace(8)", "Games necklace(7)", "Games necklace(6)", "Games necklace(5)",
                        "Games necklace(4)", "Games necklace(3)", "Games necklace(2)", "Games necklace(1)") &&
                Equipment.containsAnyOf("Ring of dueling(8)", "Ring of dueling(7)", "Ring of dueling(6)", "Ring of dueling(5)",
                        "Ring of dueling(4)", "Ring of dueling(3)", "Ring of dueling(2)", "Ring of dueling(1)") ||

                !bot.getBANK_AREA_COORDS().contains(player) &&
                        !bot.getPORTAL_OUTSIDE_COORDS().contains(player) &&
                        !bot.getPORTAL_INSIDE_COORDS().contains(player) &&
                        !bot.getBURTHORPE_COORDS().contains(player) &&
                        !bot.getTENZING_HOUSE_COORDS().contains(player) &&
                        !bot.getCLAN_WARS_TP_COORDS().contains(player) &&

                        bot.getCLAN_WARS_AREA().contains(player) &&
                        !bot.getBURTHORPE_AREA().contains(player) &&
                        !bot.isCBootsInInv() &&
                        Equipment.containsAnyOf("Games necklace(8)", "Games necklace(7)", "Games necklace(6)", "Games necklace(5)",
                                "Games necklace(4)", "Games necklace(3)", "Games necklace(2)", "Games necklace(1)") &&
                        Equipment.containsAnyOf("Ring of dueling(8)", "Ring of dueling(7)", "Ring of dueling(6)", "Ring of dueling(5)",
                                "Ring of dueling(4)", "Ring of dueling(3)", "Ring of dueling(2)", "Ring of dueling(1)");
    }

    @Override
    public void execute() {
        getLogger().debug("Traversal to portal");
        //final WebPath path = Traversal.getDefaultWeb().getPathBuilder().buildTo(bot.getPORTAL_OUTSIDE_COORDS().getRandomCoordinate());
        final BresenhamPath path = BresenhamPath.buildTo(bot.getPORTAL_OUTSIDE_COORDS().getRandomCoordinate());

        if (path != null) { // IMPORTANT: if the path should be null, the pathbuilder could not manage to build a path with the given web, so always nullcheck!
            path.step();
            Execution.delayWhile(commonAPICalls.PLAYER_MOVING, 750, 1500);
        }

    }

}