package com.Unknown.bots.UnknownGuffeFarm.CBootsRunner.Tasks;

import com.Unknown.bots.UnknownGuffeFarm.CBootsRunner.CBootsRunner;
import com.Unknown.bots.UnknownGuffeFarm.Utility.CommonAPICalls;
import com.runemate.game.api.hybrid.entities.Player;
import com.runemate.game.api.hybrid.local.hud.interfaces.Inventory;
import com.runemate.game.api.hybrid.location.navigation.Traversal;
import com.runemate.game.api.hybrid.location.navigation.basic.BresenhamPath;
import com.runemate.game.api.hybrid.location.navigation.web.WebPath;
import com.runemate.game.api.hybrid.region.Players;
import com.runemate.game.api.script.Execution;
import com.runemate.game.api.script.framework.task.Task;

public class TraversalToBank extends Task {

    private CBootsRunner bot;
    private Player player;
    private CommonAPICalls commonAPICalls;

    public TraversalToBank() {
        bot = new CBootsRunner();
        player = Players.getLocal();
        commonAPICalls = new CommonAPICalls();
    }

    @Override
    public boolean validate() {
        if (player == null) {
            return false;
        }

        return !bot.getBANK_AREA_COORDS().contains(player) &&
                !bot.getPORTAL_OUTSIDE_COORDS().contains(player) &&
                !bot.getPORTAL_INSIDE_COORDS().contains(player) &&
                !bot.getBURTHORPE_COORDS().contains(player) &&
                !bot.getTENZING_HOUSE_COORDS().contains(player) &&
                bot.getCLAN_WARS_TP_COORDS().contains(player) &&

                bot.getCLAN_WARS_AREA().contains(player) &&
                !bot.getBURTHORPE_AREA().contains(player) &&
                Inventory.isFull();
    }

    @Override
    public void execute() {
        getLogger().debug("Traversal to bank");
        //final WebPath path = Traversal.getDefaultWeb().getPathBuilder().buildTo(bot.getBANK_AREA_COORDS().getRandomCoordinate());
        final BresenhamPath path = BresenhamPath.buildTo(bot.getBANK_AREA_COORDS().getRandomCoordinate());

        if (path != null) { // IMPORTANT: if the path should be null, the pathbuilder could not manage to build a path with the given web, so always nullcheck!
            path.step();
            Execution.delayWhile(commonAPICalls.PLAYER_MOVING, 750, 1500);
        }

    }

}