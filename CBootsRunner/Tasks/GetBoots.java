package com.Unknown.bots.UnknownGuffeFarm.CBootsRunner.Tasks;

import com.Unknown.bots.UnknownGuffeFarm.CBootsRunner.CBootsRunner;
import com.runemate.game.api.hybrid.entities.Npc;
import com.runemate.game.api.hybrid.entities.Player;
import com.runemate.game.api.hybrid.local.hud.interfaces.ChatDialog;
import com.runemate.game.api.hybrid.local.hud.interfaces.InterfaceComponent;
import com.runemate.game.api.hybrid.local.hud.interfaces.Interfaces;
import com.runemate.game.api.hybrid.local.hud.interfaces.Inventory;
import com.runemate.game.api.hybrid.region.Npcs;
import com.runemate.game.api.hybrid.region.Players;
import com.runemate.game.api.script.Execution;
import com.runemate.game.api.script.framework.task.Task;

public class GetBoots extends Task {

    private CBootsRunner bot;
    private Player player;
    private Npc tenzing;
    private ChatDialog.Continue continueChat;

    public GetBoots() {
        bot = new CBootsRunner();
        player = Players.getLocal();
    }

    private void choosingOption(int option) {
        ChatDialog.Option talkingToTenzing = ChatDialog.getOption(option);
        if (talkingToTenzing != null) {
            talkingToTenzing.select(true);
        }
    }

    @Override
    public boolean validate() {
        if (player == null) {
            return false;
        }

        return !bot.getBANK_AREA_COORDS().contains(player) &&
                !bot.getPORTAL_OUTSIDE_COORDS().contains(player) &&
                !bot.getPORTAL_INSIDE_COORDS().contains(player) &&
                !bot.getBURTHORPE_COORDS().contains(player) &&
                bot.getTENZING_HOUSE_COORDS().contains(player) &&
                !bot.getCLAN_WARS_TP_COORDS().contains(player) &&

                !bot.getCLAN_WARS_AREA().contains(player) &&
                bot.getBURTHORPE_AREA().contains(player) &&
                !Inventory.isFull();
    }

    @Override
    public void execute() {
        getLogger().debug("Getting some new sneaks");
        tenzing = Npcs.newQuery().names("Tenzing").results().first();
        continueChat = ChatDialog.getContinue();

        if (ChatDialog.getOptions().size() > 0) {
            choosingOption(1);
        } else {
            if (continueChat != null && continueChat.isValid()) {
                continueChat.select(true);
            } else {
                if (tenzing != null) {
                    if (tenzing.interact("Talk-to")) {
                        Execution.delayUntil(() -> continueChat != null && continueChat.isValid(), 600, 1800);
                    }
                }
            }
        }

    }

}