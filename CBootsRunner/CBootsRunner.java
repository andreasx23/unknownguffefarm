package com.Unknown.bots.UnknownGuffeFarm.CBootsRunner;

import com.Unknown.bots.UnknownGuffeFarm.CBootsRunner.Tasks.*;
import com.Unknown.bots.UnknownGuffeFarm.Utility.CustomPlayerSense;
import com.runemate.game.api.hybrid.input.Mouse;
import com.runemate.game.api.hybrid.local.hud.interfaces.Inventory;
import com.runemate.game.api.hybrid.location.Area;
import com.runemate.game.api.hybrid.location.Coordinate;
import com.runemate.game.api.script.framework.task.TaskBot;

import java.util.regex.Pattern;

public class CBootsRunner extends TaskBot {

//    private Pattern gamesNeck, duelRing, stamPot;
    private final Area BANK_AREA_COORDS, PORTAL_OUTSIDE_COORDS, PORTAL_INSIDE_COORDS, BURTHORPE_COORDS, TENZING_HOUSE_COORDS, CLAN_WARS_TP_COORDS;
    private final Area CLAN_WARS_AREA, BURTHORPE_AREA;
    private boolean useStam;

    public CBootsRunner() {
//        gamesNeck = Pattern.compile("Games necklace\\([1-8]\\)");
//        duelRing = Pattern.compile("Ring of dueling\\([1-8]\\)");
//        stamPot = Pattern.compile("Stamina potion\\([1-4]\\)");

        BANK_AREA_COORDS = new Area.Rectangular(new Coordinate(3367, 3168, 0), new Coordinate(3372, 3173, 0));
        PORTAL_OUTSIDE_COORDS = new Area.Rectangular(new Coordinate(3351, 3161, 0), new Coordinate(3353, 3166, 0));
        PORTAL_INSIDE_COORDS = new Area.Rectangular(new Coordinate(3324, 4753, 0), new Coordinate(3330, 4751, 0));
        BURTHORPE_COORDS = new Area.Rectangular(new Coordinate(2892, 3555, 0), new Coordinate(2905, 3550, 0));
        TENZING_HOUSE_COORDS = new Area.Rectangular(new Coordinate(2818, 3557, 0), new Coordinate(2822, 3553, 0));

        CLAN_WARS_TP_COORDS = new Area.Rectangular(new Coordinate(3367, 3173, 0), new Coordinate(3391, 3154, 0));

        CLAN_WARS_AREA = new Area.Rectangular(new Coordinate(3392, 3144, 0), new Coordinate(3351, 3178, 0));
        BURTHORPE_AREA = new Area.Rectangular(new Coordinate(2908, 3545, 0), new Coordinate(2807, 3590, 0));

        useStam = false;
    }

//    public Pattern getGamesNeck() {
//        return gamesNeck;
//    }
//
//    public Pattern getDuelRing() {
//        return duelRing;
//    }
//
//    public Pattern getStamPot() {
//        return stamPot;
//    }

    public Area getBANK_AREA_COORDS() {
        return BANK_AREA_COORDS;
    }

    public Area getPORTAL_OUTSIDE_COORDS() {
        return PORTAL_OUTSIDE_COORDS;
    }

    public Area getPORTAL_INSIDE_COORDS() {
        return PORTAL_INSIDE_COORDS;
    }

    public Area getBURTHORPE_COORDS() {
        return BURTHORPE_COORDS;
    }

    public Area getTENZING_HOUSE_COORDS() {
        return TENZING_HOUSE_COORDS;
    }

    public Area getCLAN_WARS_TP_COORDS() {
        return CLAN_WARS_TP_COORDS;
    }

    ///////////////////////////////////////

    public boolean isCBootsInInv() {
        return Inventory.contains("Climbing boots");
    }

    public Area getCLAN_WARS_AREA() {
        return CLAN_WARS_AREA;
    }

    public Area getBURTHORPE_AREA() {
        return BURTHORPE_AREA;
    }

    public boolean isUseStam() {
        return useStam;
    }

    public void setUseStam(boolean useStam) {
        this.useStam = useStam;
    }

    @Override
    public void onStart(String... args) {
        CustomPlayerSense.initializeKeys();
        Mouse.setPathGenerator(Mouse.MLP_PATH_GENERATOR);

        setLoopDelay(123, 324);
        add(new EquipJewellery(), new Banking(), new TraversalToPortal(), new EnterPortal(), new TraversalToBurthhorpe(), new TraversalToTenzing(), new GetBoots(), new TraversalToClanWars(), new TraversalToBank());
    }

}
