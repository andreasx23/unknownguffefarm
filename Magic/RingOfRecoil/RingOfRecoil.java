package com.Unknown.bots.UnknownGuffeFarm.Magic.RingOfRecoil;

import com.Unknown.bots.UnknownGuffeFarm.Magic.RingOfRecoil.Tasks.Banking;
import com.Unknown.bots.UnknownGuffeFarm.Magic.RingOfRecoil.Tasks.CastSpell;
import com.Unknown.bots.UnknownGuffeFarm.Magic.RingOfRecoil.Tasks.DeselectSpell;
import com.Unknown.bots.UnknownGuffeFarm.Magic.RingOfRecoil.Tasks.TurnCameraToBanker;
import com.Unknown.bots.UnknownGuffeFarm.Utility.CustomPlayerSense;
import com.runemate.game.api.hybrid.input.Mouse;
import com.runemate.game.api.hybrid.local.Skill;
import com.runemate.game.api.script.framework.listeners.SkillListener;
import com.runemate.game.api.script.framework.listeners.events.SkillEvent;
import com.runemate.game.api.script.framework.task.TaskBot;

public class RingOfRecoil extends TaskBot implements SkillListener {

    private String jewery;
    private int sessionMagicLevel;
    private int conXP;
    private int startingMagicLevel;
    private int conLevelsGained;
    private int objectsMade;

    public RingOfRecoil() {
        jewery = "Sapphire ring";
        sessionMagicLevel = 0;
        conXP = 0;
        startingMagicLevel = Skill.MAGIC.getCurrentLevel();
        conLevelsGained = 0;
    }

    /**
     * Experience, planks used & objects made tracker
     */
    public void onExperienceGained(SkillEvent event) {
        sessionMagicLevel = Skill.MAGIC.getCurrentLevel();

        if (event.getChange() > 0) {
            conXP += event.getChange();
            objectsMade++;
        }

        if (sessionMagicLevel > startingMagicLevel) {
            conLevelsGained = sessionMagicLevel - startingMagicLevel;
        }

        System.out.println("Magic XP gained: " + conXP);
        System.out.println("Session level: " + sessionMagicLevel);
        System.out.println("Starting level: " + startingMagicLevel);
        System.out.println("Levels gained: " + conLevelsGained);
        System.out.println("Jews burned up: " + objectsMade);
    }

    @Override
    public void onStart(String... args) {
        CustomPlayerSense.initializeKeys();
        getEventDispatcher().addListener(this);
        Mouse.setPathGenerator(Mouse.MLP_PATH_GENERATOR);

        setLoopDelay(600, 1300);
        add(new TurnCameraToBanker(), new DeselectSpell(), new Banking(), new CastSpell());
    }

    public String getJewery() {
        return jewery;
    }
}