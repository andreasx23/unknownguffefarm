package com.Unknown.bots.UnknownGuffeFarm.Magic.RingOfRecoil.Tasks;

import com.runemate.game.api.hybrid.entities.GameObject;
import com.runemate.game.api.hybrid.local.Camera;
import com.runemate.game.api.hybrid.region.GameObjects;
import com.runemate.game.api.script.framework.task.Task;

public class TurnCameraToBanker extends Task {

    @Override
    public boolean validate() {
        return Camera.getYaw() == 0;
    }

    @Override
    public void execute() {
        getLogger().debug("Turning camera to banker");
        GameObject bank = GameObjects.newQuery().names("Bank booth").results().nearest();
        if (bank != null) {
            Camera.turnTo(bank);
        }
    }
}
