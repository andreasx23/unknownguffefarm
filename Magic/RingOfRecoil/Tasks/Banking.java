package com.Unknown.bots.UnknownGuffeFarm.Magic.RingOfRecoil.Tasks;

import com.Unknown.bots.UnknownGuffeFarm.Magic.RingOfRecoil.RingOfRecoil;
import com.Unknown.bots.UnknownGuffeFarm.Utility.CustomPlayerSense;
import com.runemate.game.api.hybrid.Environment;
import com.runemate.game.api.hybrid.entities.GameObject;
import com.runemate.game.api.hybrid.entities.Player;
import com.runemate.game.api.hybrid.local.hud.interfaces.Bank;
import com.runemate.game.api.hybrid.local.hud.interfaces.Equipment;
import com.runemate.game.api.hybrid.local.hud.interfaces.Inventory;
import com.runemate.game.api.hybrid.local.hud.interfaces.SpriteItem;
import com.runemate.game.api.hybrid.region.GameObjects;
import com.runemate.game.api.hybrid.region.Players;
import com.runemate.game.api.script.Execution;
import com.runemate.game.api.script.framework.task.Task;

public class Banking extends Task {

    private Player player;
    private RingOfRecoil bot;

    public Banking() {
        player = Players.getLocal();
        bot = new RingOfRecoil();
    }

    private void withdrawItem(int playerSense_REACTION_TIME, String itemToWithdraw, int itemAmountToWithdraw) {
        getLogger().debug("Withdrawing " + itemAmountToWithdraw + " " + itemToWithdraw);
        Execution.delay(playerSense_REACTION_TIME);
        if (Bank.withdraw(itemToWithdraw, itemAmountToWithdraw)) {
            Execution.delayUntil(() -> Inventory.contains(itemToWithdraw), 225, 500); //1200, 2400
        }
    }

    private void equipItem(int playerSence_REACTION_TIME, String itemName) {
        getLogger().debug("Equipping " + itemName);
        Execution.delay(playerSence_REACTION_TIME);
        if (Bank.close()) {
            Execution.delay(playerSence_REACTION_TIME);
        }
        SpriteItem item = Inventory.newQuery().names(itemName).results().first();
        if (item != null) {
            item.click();
            Execution.delayUntil(() -> !Inventory.contains(itemName), 225, 500);
        }
        Bank.open();
    }

    @Override
    public boolean validate() {
        if (player == null) {
            return false;
        }
        return !Inventory.contains(bot.getJewery()) || !Inventory.contains("Cosmic rune") || !Equipment.contains("Staff of water");
    }

    @Override
    public void execute() {
        getLogger().debug("Banking");
        GameObject bank = GameObjects.newQuery().names("Bank booth").results().nearest();
        int REACTION_TIME = CustomPlayerSense.Key.REACION_TIME.getAsInteger();

        if (bank != null) {
            if (!Bank.isOpen()) {
                Bank.open();
            } else {
                Bank.depositAllExcept("Cosmic rune", bot.getJewery());
                Execution.delay(REACTION_TIME);

                if (!Bank.contains("Staff of water") && !Equipment.contains("Staff of water")) {
                    Environment.getBot().pause("Dosen't have staff of water");
                }
                if (!Equipment.contains("Staff of water")) {
                    withdrawItem(REACTION_TIME, "Staff of water", 1);
                    equipItem(REACTION_TIME, "Staff of water");
                }

                if (!Bank.contains("Cosmic rune") && !Inventory.contains("Cosmic rune")) {
                    Environment.getBot().pause("No more cosmic runes");
                }
                if (!Inventory.contains("Cosmic rune")) {
                    withdrawItem(REACTION_TIME, "Cosmic rune", 1000);
                }

                if (!Bank.contains(bot.getJewery()) && !Inventory.contains(bot.getJewery())) {
                    Environment.getBot().pause("No more " + bot.getJewery());
                }
                if (!Inventory.contains(bot.getJewery())) {
                    withdrawItem(REACTION_TIME, bot.getJewery(), 1000);
                }

                if (Inventory.contains(bot.getJewery()) && Inventory.contains("Cosmic rune") && Equipment.contains("Staff of water")) {
                    if (CustomPlayerSense.Key.USE_ESCAPE_TO_CLOSE_BANK.getAsBoolean()) {
                        getLogger().debug("USED ESCAPE!");
                        Bank.close(true);
                    } else {
                        getLogger().debug("MANUALLY CLOSED!");
                        Bank.close(false);
                    }
                }
            }
        }
    }
}
