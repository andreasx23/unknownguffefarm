package com.Unknown.bots.UnknownGuffeFarm.Magic.RingOfRecoil.Tasks;

import com.Unknown.bots.UnknownGuffeFarm.Magic.RingOfRecoil.RingOfRecoil;
import com.Unknown.bots.UnknownGuffeFarm.Utility.CustomPlayerSense;
import com.runemate.game.api.hybrid.input.Mouse;
import com.runemate.game.api.hybrid.local.hud.interfaces.Inventory;
import com.runemate.game.api.osrs.local.hud.interfaces.Magic;
import com.runemate.game.api.script.Execution;
import com.runemate.game.api.script.framework.task.Task;

public class DeselectSpell extends Task {

    private RingOfRecoil bot;

    public DeselectSpell() {
        bot = new RingOfRecoil();
    }

    @Override
    public boolean validate() {
        return Magic.LVL_1_ENCHANT.isSelected() && !Inventory.contains(bot.getJewery());
    }

    @Override
    public void execute() {
        getLogger().debug("Deselecting spell");
        int REACTION_TIME = CustomPlayerSense.Key.REACION_TIME.getAsInteger();

        if (Mouse.click(Mouse.Button.LEFT)) {
            Execution.delay(REACTION_TIME);
        }
    }
}
