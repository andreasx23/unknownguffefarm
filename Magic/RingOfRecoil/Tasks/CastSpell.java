package com.Unknown.bots.UnknownGuffeFarm.Magic.RingOfRecoil.Tasks;

import com.Unknown.bots.UnknownGuffeFarm.Magic.RingOfRecoil.RingOfRecoil;
import com.Unknown.bots.UnknownGuffeFarm.Utility.CommonAPICalls;
import com.Unknown.bots.UnknownGuffeFarm.Utility.CustomPlayerSense;
import com.runemate.game.api.hybrid.entities.Player;
import com.runemate.game.api.hybrid.input.Keyboard;
import com.runemate.game.api.hybrid.input.Mouse;
import com.runemate.game.api.hybrid.local.Screen;
import com.runemate.game.api.hybrid.local.hud.InteractablePoint;
import com.runemate.game.api.hybrid.local.hud.interfaces.*;
import com.runemate.game.api.hybrid.region.Players;
import com.runemate.game.api.hybrid.util.calculations.Random;
import com.runemate.game.api.osrs.local.hud.interfaces.Magic;
import com.runemate.game.api.script.Execution;
import com.runemate.game.api.script.framework.task.Task;

import java.awt.event.KeyEvent;

public class CastSpell extends Task {

    private CommonAPICalls calls;
    private Player player;
    private RingOfRecoil bot;
    private int inventoryId;

    public CastSpell() {
        bot = new RingOfRecoil();
        calls = new CommonAPICalls();
        player = Players.getLocal();
    }

    private int getOffScreenX() {
        if (Random.nextBoolean()) {
            final int width = Screen.getBounds().width;
            return width + Random.nextInt(20, 50);
        } else {
            return 0 - Random.nextInt(20, 50);
        }
    }

    private int getOffScreenY() {
        if (Random.nextBoolean()) {
            final int height = Screen.getBounds().height;
            return height + Random.nextInt(20, 50);
        } else {
            return 0 - Random.nextInt(20, 50);
        }
    }

    private InterfaceComponent getInventoryComponent() {
        InterfaceComponent comp;
        if (inventoryId != 0) {
            if ((comp = Interfaces.getAt(548, inventoryId)) != null) {
                return comp;
            }
        } else {
            if ((comp = Interfaces.newQuery().containers(548).actions("Inventory").results().first()) != null) {
                inventoryId = comp.getIndex();
                return comp;
            }
        }
        return null;
    }

    private void openInventory() {
        if (CustomPlayerSense.Key.USE_ESCAPE_TO_OPEN_INV_TAP.getAsBoolean()) {
            getLogger().debug("Showing inventory interface by using escape");
            Execution.delayUntil(()-> calls.isAnimating(player), 1250, 2500);
            Keyboard.pressKey(KeyEvent.VK_ESCAPE);
        } else {
            if (getInventoryComponent() != null) {
                getLogger().debug("Showing inventory interface");
                Execution.delayUntil(()-> calls.isAnimating(player), 1250, 2500);
                getInventoryComponent().interact("Inventory");
            }
        }
    }

    @Override
    public boolean validate() {
        if (player == null) {
            return false;
        }
        return Inventory.contains("Cosmic rune") && Inventory.contains(bot.getJewery()) && Equipment.contains("Staff of water");
    }

    @Override
    public void execute() {
        SpriteItem jewelry = Inventory.newQuery().names(bot.getJewery()).results().sortByDistanceFromMouse().first();
        getLogger().debug("Casting Spell");
        if (!Magic.LVL_1_ENCHANT.isSelected()) {
            getLogger().debug("Selecting lvl 1 enchant");
            Magic.LVL_1_ENCHANT.activate();
        } else {
            getLogger().debug("Using spell on ring");
            if (jewelry != null) {
                getLogger().debug("Jewel != null");
                if (jewelry.click()) {
                    openInventory();
                    getLogger().debug("Moving mouse off screen");
                    Mouse.move(new InteractablePoint(getOffScreenX(), getOffScreenY()));
                    getLogger().debug("Executing delay");
                    Execution.delayUntil(()-> !Inventory.contains(bot.getJewery()), calls.PLAYER_ANIMATING, 300, 3000, 5000);
                }
            }
        }
    }
}
