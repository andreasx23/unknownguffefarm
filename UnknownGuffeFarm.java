package com.Unknown.bots.UnknownGuffeFarm;

import com.Unknown.bots.UnknownGuffeFarm.CBootsRunner.Tasks.*;
import com.Unknown.bots.UnknownGuffeFarm.Utility.CustomPlayerSense;
import com.runemate.game.api.hybrid.input.Mouse;
import com.runemate.game.api.script.framework.task.TaskBot;

public class UnknownGuffeFarm extends TaskBot {

    @Override
    public void onStart(String... args){
        CustomPlayerSense.initializeKeys();
        Mouse.setPathGenerator(Mouse.MLP_PATH_GENERATOR);

        setLoopDelay(123, 324);
        add();
    }

}