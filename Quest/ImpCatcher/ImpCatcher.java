package com.Unknown.bots.UnknownGuffeFarm.Quest.ImpCatcher;

import com.Unknown.bots.UnknownGuffeFarm.Quest.ImpCatcher.Tasks.RestartBotOnComplete;
import com.Unknown.bots.UnknownGuffeFarm.Quest.ImpCatcher.Tasks.TalkingWithMizgog;
import com.Unknown.bots.UnknownGuffeFarm.Quest.ImpCatcher.Tasks.TraversalTower;
import com.Unknown.bots.UnknownGuffeFarm.Utility.CustomPlayerSense;
import com.runemate.game.api.hybrid.input.Mouse;
import com.runemate.game.api.script.framework.task.TaskBot;

public class ImpCatcher extends TaskBot {

    @Override
    public void onStart(String... args) {
        CustomPlayerSense.initializeKeys();
        Mouse.setPathGenerator(Mouse.MLP_PATH_GENERATOR);

        setLoopDelay(600, 1300);
        add(new RestartBotOnComplete(), new TalkingWithMizgog(), new TraversalTower());
    }
}
