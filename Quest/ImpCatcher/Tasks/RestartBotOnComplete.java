package com.Unknown.bots.UnknownGuffeFarm.Quest.ImpCatcher.Tasks;

import com.runemate.game.api.hybrid.Environment;
import com.runemate.game.api.hybrid.local.Varp;
import com.runemate.game.api.hybrid.local.Varps;
import com.runemate.game.api.script.framework.task.Task;

public class RestartBotOnComplete extends Task {

    private Varp impCatcherVarp;

    public RestartBotOnComplete() {
        impCatcherVarp = Varps.getAt(160);
    }

    @Override
    public boolean validate() {
        return impCatcherVarp.getValue() == 2;
    }

    @Override
    public void execute() {
        Environment.restart();
    }
}
