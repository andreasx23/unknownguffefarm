package com.Unknown.bots.UnknownGuffeFarm.Quest.ImpCatcher.Tasks;

import com.runemate.game.api.hybrid.entities.Npc;
import com.runemate.game.api.hybrid.entities.Player;
import com.runemate.game.api.hybrid.local.Camera;
import com.runemate.game.api.hybrid.local.Varp;
import com.runemate.game.api.hybrid.local.Varps;
import com.runemate.game.api.hybrid.local.hud.interfaces.ChatDialog;
import com.runemate.game.api.hybrid.location.Area;
import com.runemate.game.api.hybrid.location.Coordinate;
import com.runemate.game.api.hybrid.region.Npcs;
import com.runemate.game.api.hybrid.region.Players;
import com.runemate.game.api.script.Execution;
import com.runemate.game.api.script.framework.task.Task;

public class TalkingWithMizgog extends Task {

    private Player player;
    private Area topArea;
    private Varp impCatcherVarp;
    private ChatDialog.Continue continueChat;

    public TalkingWithMizgog() {
        player = Players.getLocal();
        topArea = Area.rectangular(new Coordinate(3100, 3175, 2), new Coordinate(3115, 3150, 2));
        impCatcherVarp = Varps.getAt(160);
        System.out.println(impCatcherVarp.getValue());
    }

    private void choosingOption(int option) {
        ChatDialog.Option talkingToNpc = ChatDialog.getOption(option);
        if (talkingToNpc != null) {
            talkingToNpc.select(true);
        }
    }

    @Override
    public boolean validate() {
        if (player == null) {
            return false;
        }
        return topArea.contains(player) && impCatcherVarp.getValue() != 2;
    }

    @Override
    public void execute() {
        System.out.println(impCatcherVarp.getValue() + "nignaign");
        Npc mizgog = Npcs.newQuery().ids(5005).results().first();
        continueChat = ChatDialog.getContinue();

        if (mizgog != null) {
            getLogger().debug("Mizgog != null");
            if (!mizgog.isVisible()) {
                Camera.turnTo(mizgog);
            }
            if (ChatDialog.getOptions().size() > 0) {
                choosingOption(1);
            } else {
                if (continueChat != null && continueChat.isValid()) {
                    continueChat.select(true);
                } else {
                    if (mizgog.interact("Talk-to")) {
                        Execution.delayUntil(() -> continueChat != null && continueChat.isValid(), 2500, 3500);
                    }
                }
            }
        }
    }
}
