package com.Unknown.bots.UnknownGuffeFarm.Quest.ImpCatcher.Tasks;

import com.runemate.game.api.hybrid.entities.GameObject;
import com.runemate.game.api.hybrid.entities.Player;
import com.runemate.game.api.hybrid.local.hud.interfaces.Inventory;
import com.runemate.game.api.hybrid.location.Area;
import com.runemate.game.api.hybrid.location.Coordinate;
import com.runemate.game.api.hybrid.location.navigation.Traversal;
import com.runemate.game.api.hybrid.location.navigation.web.WebPath;
import com.runemate.game.api.hybrid.region.GameObjects;
import com.runemate.game.api.hybrid.region.Players;
import com.runemate.game.api.script.Execution;
import com.runemate.game.api.script.framework.task.Task;

public class TraversalTower extends Task {

    private Player player;
    private Area topArea;

    public TraversalTower() {
        player = Players.getLocal();
        topArea = Area.rectangular(new Coordinate(3103, 3165, 2), new Coordinate(3107, 3157, 2));
    }

    @Override
    public boolean validate() {
        if (player == null) {
            return false;
        }
        return Inventory.contains("Black bead") && Inventory.contains("Red bead") && Inventory.contains("White bead") && Inventory.contains("Yellow bead") && !topArea.contains(player);
    }

    @Override
    public void execute() {
        Area outsideTower = Area.rectangular(new Coordinate(3105, 3173, 0), new Coordinate(3112, 3167, 0));
        Area insideTowerLower = Area.rectangular(new Coordinate(3111, 3166, 0), new Coordinate(3103, 3158, 0));
        Area medTowerArea = Area.rectangular(new Coordinate(3103, 3165, 1), new Coordinate(3107, 3157, 1));
        WebPath pathToTower = Traversal.getDefaultWeb().getPathBuilder().buildTo(outsideTower.getRandomCoordinate());
        GameObject door1 = GameObjects.newQuery().names("Door").actions("Open").on(new Coordinate(3109, 3167, 0)).results().first();
        GameObject door2 = GameObjects.newQuery().names("Door").actions("Open").on(new Coordinate(3107, 3162, 0)).results().first();


        if (!outsideTower.contains(player) && !outsideTower.contains(player) && !medTowerArea.contains(player)) {
            if (pathToTower != null) {
                pathToTower.step();
            }
        }
        if (door1 != null && outsideTower.contains(player)) {
            door1.interact("Open");
            Execution.delay(1500, 3000);
        }
        if (door2 != null && insideTowerLower.contains(player) || door1 == null && outsideTower.contains(player)) {
            door2.interact("Open");
            Execution.delay(2500, 3500);
        }
        if (door1 == null && door2 == null && insideTowerLower.contains(player)) {
            GameObject stair = GameObjects.newQuery().names("Staircase").results().nearest();
            if (stair != null) {
                stair.interact("Climb-up");
                Execution.delay(1500, 3000);
            }
        }
        if (medTowerArea.contains(player)) {
            GameObject stair = GameObjects.newQuery().names("Staircase").results().nearest();
            stair.interact("Climb-up");
            Execution.delay(1500, 3000);
        }
    }
}
