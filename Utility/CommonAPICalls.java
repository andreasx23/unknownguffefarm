package com.Unknown.bots.UnknownGuffeFarm.Utility;

import com.runemate.game.api.hybrid.entities.Player;
import com.runemate.game.api.hybrid.region.Players;
import com.runemate.game.api.script.Execution;

import java.util.concurrent.Callable;

public class CommonAPICalls {

    /**
     * Returns if the player is moving or not
     */
    public Callable<Boolean> PLAYER_MOVING = () -> {
        final Player player = Players.getLocal();
        return player != null && player.isMoving();
    };

    /**
     * Returns if the player is animating or not
     */
    public Callable<Boolean> PLAYER_ANIMATING = () -> {
        final Player player = Players.getLocal();
        return player != null && player.getAnimationId() != -1;
    };

    /**
     * Returns if the player is animating or not
     */
    public boolean isAnimating(Player player){
        return player != null && player.getAnimationId() != -1;
    }

    public boolean isMining() {
        return Execution.delayUntil(PLAYER_ANIMATING, 1200);
    }

}
