package com.Unknown.bots.UnknownGuffeFarm.Utility;

import com.runemate.game.api.hybrid.player_sense.PlayerSense;
import com.runemate.game.api.hybrid.util.calculations.Random;

import java.util.function.Supplier;

public class CustomPlayerSense {

    public static void initializeKeys() {
        for (Key key : Key.values()) {
            if (PlayerSense.get(key.name) == null) {
                PlayerSense.put(key.name, key.supplier.get());
            }
        }
    }

    public enum Key {
        CAMERA_PITCH("Unknown Camera Pitch", () -> Random.nextDouble(0.665, 0.715)),
        CAMERA_YAW("Unknown Camera Yaw", () -> Random.nextInt(87, 93)),
        REACION_TIME_TRAVERSE("Unknown reaction time travelling", () -> Random.nextLong(360, 860)),
        REACION_TIME_DRROPPING("Unknown reaction time dropping", () -> Random.nextLong(35, 65)),
        SPAM_CLICK_COUNTER_TO_BRAZIER("Unknown spam click counter to brazier", () -> Random.nextInt(2, 6)),
        RANDOM_WINTERTODT_SPAWNTIMER("Unknown random spawn timer for the wintertodt", () -> Random.nextInt(1, 4)),
        USE_ESCAPE_TO_CLOSE_BANK("Unknown use escape to close bank or not", () -> Random.nextBoolean()),
        USE_ESCAPE_TO_OPEN_INV_TAP("Unknown use escape to open inventory or not", () -> Random.nextBoolean()),

        SHIFT_DROP_MOUSE_MOVING_TIMEOUT("p_shift_drop_mouse_moving_timeout_average", () -> Random.nextLong(100, 600)),
        SHIFT_DROP_RELEASE_DELAY("p_shift_drop_release_delay", () -> Random.nextLong(200, 700)),
        FAST_MOUSE_CLICK_DELAY_AVERAGE("p_fast_mouse_click_delay_average", () -> Random.nextLong(30, 60)),

        ACTIVENESS_FACTOR_WHILE_WAITING("prime_activeness_factor", () -> Random.nextDouble(0.2, 0.8)),
        SPAM_CLICK_COUNT("prime_spam_click_count", () -> Random.nextInt(2, 6)),
        REACION_TIME("prime_reaction_time", () -> Random.nextLong(160, 260)),
        SPAM_CLICK_TO_HEAL("prime_spam_healing", () -> Random.nextBoolean());

        private final String name;
        private final Supplier supplier;

        Key(String name, Supplier supplier) {
            this.name = name;
            this.supplier = supplier;
        }

        public String getKey() {
            return name;
        }

        public Integer getAsInteger() {
            return PlayerSense.getAsInteger(name);
        }

        public Double getAsDouble() {
            return PlayerSense.getAsDouble(name);
        }

        public Long getAsLong() {
            return PlayerSense.getAsLong(name);
        }

        public Boolean getAsBoolean() {
            return PlayerSense.getAsBoolean(name);
        }
    }
}
